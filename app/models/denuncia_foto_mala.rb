class DenunciaFotoMala < ApplicationRecord

  belongs_to :usuario, optional: false, validate: true
  belongs_to :fotoe14, optional: false, validate: true

  enum motivo: {
    ilegible: 0,
    incompleta: 1,
    falsa: 2,
    prueba: 3,
    invalida: 4
  }
end
