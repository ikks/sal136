class CambiaTipoPuesto < ActiveRecord::Migration[7.0]
  def up
    change_column :tabulae14, :puesto, :string, limit: 2, null: false
  end

  def down
    change_column :tabulae14, :puesto, :integer, null: false
  end
end
