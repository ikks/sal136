class AgregaGeoelectoralEleccion < ActiveRecord::Migration[7.0]
  def up
    add_reference :eleccion, :geoelectoral, foreign_key: true
    e = Eleccion.find(1)
    e.geoelectoral_id = 1
    e.save!
    change_column_null :eleccion, :geoelectoral_id, from: true, to: false
  end

  def down
    remove_reference :eleccion, :geoelectoral
  end
end
