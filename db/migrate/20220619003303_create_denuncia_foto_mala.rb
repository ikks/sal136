class CreateDenunciaFotoMala < ActiveRecord::Migration[7.0]
  def change
    create_table :denuncia_foto_mala do |t|
      t.integer :motivo, null: false, default: 0
      t.timestamps
    end
    add_reference :denuncia_foto_mala, :fotoe14, foreign_key: true
    add_reference :denuncia_foto_mala, :usuario, foreign_key: true
  end
end
