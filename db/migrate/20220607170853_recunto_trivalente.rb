class RecuntoTrivalente < ActiveRecord::Migration[7.0]
  def up
    add_column :tabulae14, :recuento_id, :integer, default: 1
    add_foreign_key :tabulae14, :sip_trivalente, column: :recuento_id
    execute <<-SQL
      UPDATE tabulae14 SET 
        recuento_id=CASE WHEN recuento='t' THEN 2
          WHEN recuento='f' THEN 3
          ELSE 1 
        END 
        ;
    SQL
    remove_column :tabulae14, :recuento
  end

  def down
    add_column :tabulae14, :recuento, :boolean
    execute <<-SQL
      UPDATE tabulae14 SET 
        recuento=CASE WHEN recuento_id=2 THEN 't'
          WHEN recuento_id=3 THEN 'f'
          ELSE NULL
        END 
        ;
    SQL
    remove_column :tabulae14, :recuento_id
  end
end
