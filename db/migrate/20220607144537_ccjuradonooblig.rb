class Ccjuradonooblig < ActiveRecord::Migration[7.0]
  def change
    change_column_null :tabulae14, :ccjurado1, from: false, to: true
    change_column_null :tabulae14, :ccjurado2, from: false, to: true
  end
end
