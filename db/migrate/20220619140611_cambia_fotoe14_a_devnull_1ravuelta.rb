class CambiaFotoe14ADevnull1ravuelta < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL
      UPDATE fotoe14 SET 
        url=REGEXP_REPLACE(url, '^https://app.fotoe14.co/static/images/(.*_PRE_X_)([0-9][0-9])_([0-9][0-9][0-9])_([0-9][0-9][0-9])(_XX_.*.pdf)', 'https://devnull.li/e14/pres_2022_1eravuelta/\\2/\\3/\\4/PRE/\\1\\2_\\3_\\4\\5') ;
    SQL
  end
  def down
    execute <<-SQL
      UPDATE fotoe14 SET
        url=REGEXP_REPLACE(url, '^https://devnull.li/e14/pres_2022_1eravuelta/../.../.../PRE/(.*)', 'https://app.fotoe14.co/static/images/\\1') ;
    SQL
  end
end
