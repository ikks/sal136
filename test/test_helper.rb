ENV['RAILS_ENV'] ||= 'test'

require 'simplecov'
SimpleCov.start 'rails'
require_relative '../config/environment'
require 'rails/test_help'
require 'rake'

Rake::Task.clear # necessary to avoid tasks being loaded several times in dev mode
Au2022::Application.load_tasks 

class ActiveSupport::TestCase
  #fixtures :all

  if Sip::Tclase.all.count == 0
    load "#{Rails.root}/db/seeds.rb"
    Rake::Task['sip:indices'].invoke
  end

  protected
  def load_seeds
    load "#{Rails.root}/db/seeds.rb"
  end
end

# Usuario para ingresar y hacer pruebas
PRUEBA_USUARIO = {
  nusuario: "admin",
  password: "sjrven123",
  nombres: "roberto",
  apellidos: "perez",
  descripcion: "admin",
  rol: 1,
  idioma: "es_CO",
  email: "usuario1@localhost",
  encrypted_password: '$2a$10$uMAciEcJuUXDnpelfSH6He7BxW0yBeq6VMemlWc5xEl6NZRDYVA3G',
  sign_in_count: 0,
  fechacreacion: "2014-08-05",
  fechadeshabilitacion: nil,
}


PRUEBA_ELECCION= {
  id: 100,
  nombre: 'x1',
  numcandidatos: 2,
  fechaini: '2022-01-01',
  fechafin: '2022-01-02',
  candidato1: 'c1',
  candidato2: 'c2'
}

PRUEBA_TABULAE14 = {
  id: 10000001,
  usuario_id: 1,
  eleccion_id: 1,
  municipio_id: 1,
  zona: 1, 
  puesto: 1, 
  mesa: 1, 
  totalsufragantes: 1, 
  totalurna: 1, 
  totalincinerados: 1, 
  c1: 1,
  c2: 1,
  blanco: 1, 
  nulos: 1, 
  nomarcados: 1, 
  votosmesa: 1,
  ccjurado1: 1, 
  ccjurado2: 1,
  created_at: '2022-06-04',
  updated_at: '2022-06-04'
}


