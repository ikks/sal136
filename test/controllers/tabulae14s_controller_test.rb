require "test_helper"

class Tabulae14sControllerTest < ActionDispatch::IntegrationTest

  include Devise::Test::IntegrationHelpers

  setup  do
    if ENV['CONFIG_HOSTS'] != 'www.example.com'
      raise 'CONFIG_HOSTS debe ser www.example.com'
    end
    @current_usuario = ::Usuario.find(1)
    sign_in @current_usuario
    @tabulae14 = ::Tabulae14.create!(PRUEBA_TABULAE14)
  end

  # Cada prueba que se ejecuta se hace en una transacción
  # que después de la prueba se revierte

  test "debe presentar listado" do
    get tabulae14s_path
    assert_response :success
    assert_template :index
  end

  test "debe presentar resumen de existente" do
    get tabulae14_url(@tabulae14.id)
    assert_response :success
    assert_template :show
  end

  test "debe presentar formulario para nueva" do
    get new_tabulae14_path
    assert_response :success
    assert_template :new
  end

  test "debe presentar formulario de edición" do
    get edit_tabulae14_path(@tabulae14)
    assert_response :success
    assert_template :edit
  end

  test "debe crear nueva" do
    # Arreglamos indice
    ::Tabulae14.connection.execute <<-SQL
        SELECT setval('public.tabulae14_id_seq', MAX(id)) 
          FROM public.tabulae14;
    SQL
    assert_difference('Tabulae14.count') do
      post tabulae14s_path, params: { 
        tabulae14: { 
          id: nil,
          nombre: 'ZZ',
          numcandidatos: 3,
          candidato1: 't1',
          candidato2: 't2',
          candidato3: 't3',
          fechaini_localizada: '2/Feb/2022',
          fechafin_localizada: '3/Feb/2022'
        }
      }
    end

    assert_redirected_to main_app.tabulae14_path(
      assigns(:tabulae14))
  end

  test "debe actualizar existente" do
    patch main_app.tabulae14_path(@tabulae14.id),
      params: { 
      tabulae14: { 
        id: @tabulae14.id,
        nombre: 'YY'
      }
    }

      assert_redirected_to main_app.tabulae14_path(assigns(:tabulae14))
  end

  test "debe eliminar" do
    assert_difference('Tabulae14.count', -1) do
      delete main_app.tabulae14_path(Tabulae14.find(1))
    end

    assert_redirected_to main_app.tabulae14s_path
  end

end
