require "application_system_test_case"

class Tabulae14sTest < ApplicationSystemTestCase
  setup do
    @tabulae14 = tabulae14(:one)
  end

  test "visiting the index" do
    visit tabulae14s_url
    assert_selector "h1", text: "Tabulae14s"
  end

  test "should create tabulae14" do
    visit tabulae14s_url
    click_on "New tabulae14"

    fill_in "Blanco", with: @tabulae14.blanco
    fill_in "C1", with: @tabulae14.c1
    fill_in "C10", with: @tabulae14.c10
    fill_in "C11", with: @tabulae14.c11
    fill_in "C12", with: @tabulae14.c12
    fill_in "C2", with: @tabulae14.c2
    fill_in "C3", with: @tabulae14.c3
    fill_in "C4", with: @tabulae14.c4
    fill_in "C5", with: @tabulae14.c5
    fill_in "C6", with: @tabulae14.c6
    fill_in "C7", with: @tabulae14.c7
    fill_in "C8", with: @tabulae14.c8
    fill_in "C9", with: @tabulae14.c9
    fill_in "Ccjurado1", with: @tabulae14.ccjurado1
    fill_in "Ccjurado2", with: @tabulae14.ccjurado2
    fill_in "Ccjurado3", with: @tabulae14.ccjurado3
    fill_in "Ccjurado4", with: @tabulae14.ccjurado4
    fill_in "Ccjurado5", with: @tabulae14.ccjurado5
    fill_in "Ccjurado6", with: @tabulae14.ccjurado6
    fill_in "Enrepde", with: @tabulae14.enrepde
    fill_in "Mesa", with: @tabulae14.mesa
    fill_in "Municipio", with: @tabulae14.municipio_id
    fill_in "Nomarcados", with: @tabulae14.nomarcados
    fill_in "Nulos", with: @tabulae14.nulos
    fill_in "Otrasconstancias", with: @tabulae14.otrasconstancias
    fill_in "Puesto", with: @tabulae14.puesto
    check "Recuento" if @tabulae14.recuento
    fill_in "Solicitadopor", with: @tabulae14.solicitadopor
    fill_in "Totalincinerados", with: @tabulae14.totalincinerados
    fill_in "Totalsufragantes", with: @tabulae14.totalsufragantes
    fill_in "Totalurna", with: @tabulae14.totalurna
    fill_in "Usuario", with: @tabulae14.usuario_id
    fill_in "Votosmesa", with: @tabulae14.votosmesa
    fill_in "Zona", with: @tabulae14.zona
    click_on "Create Tabulae14"

    assert_text "Tabulae14 was successfully created"
    click_on "Back"
  end

  test "should update Tabulae14" do
    visit tabulae14_url(@tabulae14)
    click_on "Edit this tabulae14", match: :first

    fill_in "Blanco", with: @tabulae14.blanco
    fill_in "C1", with: @tabulae14.c1
    fill_in "C10", with: @tabulae14.c10
    fill_in "C11", with: @tabulae14.c11
    fill_in "C12", with: @tabulae14.c12
    fill_in "C2", with: @tabulae14.c2
    fill_in "C3", with: @tabulae14.c3
    fill_in "C4", with: @tabulae14.c4
    fill_in "C5", with: @tabulae14.c5
    fill_in "C6", with: @tabulae14.c6
    fill_in "C7", with: @tabulae14.c7
    fill_in "C8", with: @tabulae14.c8
    fill_in "C9", with: @tabulae14.c9
    fill_in "Ccjurado1", with: @tabulae14.ccjurado1
    fill_in "Ccjurado2", with: @tabulae14.ccjurado2
    fill_in "Ccjurado3", with: @tabulae14.ccjurado3
    fill_in "Ccjurado4", with: @tabulae14.ccjurado4
    fill_in "Ccjurado5", with: @tabulae14.ccjurado5
    fill_in "Ccjurado6", with: @tabulae14.ccjurado6
    fill_in "Enrepde", with: @tabulae14.enrepde
    fill_in "Mesa", with: @tabulae14.mesa
    fill_in "Municipio", with: @tabulae14.municipio_id
    fill_in "Nomarcados", with: @tabulae14.nomarcados
    fill_in "Nulos", with: @tabulae14.nulos
    fill_in "Otrasconstancias", with: @tabulae14.otrasconstancias
    fill_in "Puesto", with: @tabulae14.puesto
    check "Recuento" if @tabulae14.recuento
    fill_in "Solicitadopor", with: @tabulae14.solicitadopor
    fill_in "Totalincinerados", with: @tabulae14.totalincinerados
    fill_in "Totalsufragantes", with: @tabulae14.totalsufragantes
    fill_in "Totalurna", with: @tabulae14.totalurna
    fill_in "Usuario", with: @tabulae14.usuario_id
    fill_in "Votosmesa", with: @tabulae14.votosmesa
    fill_in "Zona", with: @tabulae14.zona
    click_on "Update Tabulae14"

    assert_text "Tabulae14 was successfully updated"
    click_on "Back"
  end

  test "should destroy Tabulae14" do
    visit tabulae14_url(@tabulae14)
    click_on "Destroy this tabulae14", match: :first

    assert_text "Tabulae14 was successfully destroyed"
  end
end
