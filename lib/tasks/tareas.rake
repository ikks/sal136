# encoding: UTF-8

require 'active_support/core_ext/object/inclusion'
require 'active_record'
require 'colorize'

#require 'byebug'

#require_relative '../../app/helpers/sip/tareasrake_helper'

desc "Vuelca datosindices"
task vuelcadatos: :environment do
  connection = ActiveRecord::Base.connection();
  puts "datos"

  maq = '-h ' + ENV.fetch('BD_SERVIDOR') + ' -U ' + ENV.fetch('BD_USUARIO')
  Sip::TareasrakeHelper::asegura_varambiente_bd
  connection = ActiveRecord::Base.connection()
  nomarc = "db/datos.sql"
  File.open(nomarc, "w") { |f| 
    f << "-- Volcado de datos de elecciones populares en Colombia tabulados por voluntarios en https://sal136.pasosdeJesus.org\n"
    f << "-- Dominio público de acuerdo a la legislación colombiana.  https://www.pasosdejesus.org/dominio_publico_colombia.html\n\n"
  }
  puts "usuario"
  command = "pg_dump --inserts --data-only --no-privileges " +
    "--no-owner --column-inserts --table=usuario " +
    "#{maq} #{Shellwords.escape(ENV.fetch('BD_NOMBRE'))} " +
    "| grep -v \"INTO public.usuario (nusuario, password, nombres, descripcion, rol, idioma, id, fechacreacion, fechadeshabilitacion, email, encrypted_password,.*([^,]*, [^,]*, [^,]*, [^,]*, [^,]*, [^,]*, 1,.*\" " +
    "| sed -e \"s/\\(.*INTO public.usuario (nusuario, password, nombres, descripcion, rol, idioma, id, fechacreacion, fechadeshabilitacion, email, encrypted_password,.*(\\)[^,]*\\(, [^,]*, [^,]*, [^,]*, [^,]*, [^,]*, \\)\\([^,]*\\)\\(, [^,]*, [^,]*, \\)[^,]*, [^,]*\\(,.*)\\)/\\1'correo\\3_privado_co'\\2\\3\\4'correo\\3@privado.co', 'sin condensado'\\5/g\" " +
    "| sed -e \"s/SET lock_timeout = 0;//g\" >> #{nomarc}"
  puts command.green
  raise "Error al volcar tabla usuario" unless Kernel.system(command)
  ['sip_departamento', 'sip_municipio', 'geoelectoral',
   'zona', 'puesto', 'mesa', 'eleccion', 'fotoe14', 'tabulae14'].each do |tabla|
    puts tabla
    command = "pg_dump --inserts --data-only --no-privileges " +
      "--no-owner --column-inserts --table=#{tabla} " +
      "#{maq} #{Shellwords.escape(ENV.fetch('BD_NOMBRE'))} " +
      "| sed -e \"s/SET lock_timeout = 0;//g\" >> #{nomarc}"
    puts command.green
    raise "Error al volcar tabla #{tabla}" unless Kernel.system(command)
  end


end



